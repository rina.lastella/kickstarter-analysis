# 2022 Jun: Kickstarter Campaign Analysis
 DABC class, Kickstarter sample data. Module 1.
 Excel tables, pivot tables, and graphs.
 
 
 - The month of May has shown the highest number of successful outcomes.
 
 ![Outcomes Based on Launch Date.png](https://github.com/laurinalastella/kickstarter-analysis/blob/main/Outcomes%20Based%20on%20Launch%20Date.png?raw=true)
 
 
 - Among the categories, theater has the highest number of successful campaigns.
 
 ![Parent Category Outcomes.png](https://github.com/laurinalastella/kickstarter-analysis/blob/main/Parent%20Category%20Outcomes.png?raw=true)
 
 
 
 
 - In both the US and GB, plays are the most successful sub-category of theater campaigns.
 
 ![Subcategory Statistics US.png](https://github.com/laurinalastella/kickstarter-analysis/blob/main/Subcategory%20Statistics%20US.png?raw=true)
 
 
 ![Subcategory Statistics GB.png](https://github.com/laurinalastella/kickstarter-analysis/blob/main/Subcategory%20Statistics%20GB.png?raw=true)
 
 
 Observations:
 - Your five favorite Edinburgh plays had a minimum goal (in GB Pounds) of 1000, and a maximum goal of 4000.
 - They had an average goal of approximately 2000 GBP, and all of their pledges were over their goals.
 
 Recommendations:
 - Aim your campaign goal to be between $3000-$5000.
 - May is the best month to launch a campaign.
 
 
 
